/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package janken;

import janken.Enum.ResultTypeEnum;

/**
 *
 * @author mazu
 */
public class ResultCounter {
    private int winCount;
    private int loseCount;
    private int drawCount;
    
    public ResultCounter()
    {
        this.drawCount = 0;
        this.loseCount = 0;
        this.winCount = 0;
    }
    
    public int getBattleCount(){
        return this.drawCount + this.loseCount + this.winCount;
    }
    
    public int getWinCount(){
        return this.winCount;
    }
    
    public int getLoseCount(){
        return this.loseCount;
    }
    
    public int getDrawCount(){
        return this.drawCount;
    }
    
    public void count(ResultTypeEnum type){
        if(type == ResultTypeEnum.WIN){
            ++this.winCount;
        }else if(type == ResultTypeEnum.LOSE){
            ++this.loseCount;
        }else if(type == ResultTypeEnum.DRAW){
           ++this.drawCount;
        }
    }
}
