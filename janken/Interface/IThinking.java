/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package janken.Interface;

import janken.Enum.HandTypeEnum;

/**
 *
 * @author usuda2102
 */
public interface IThinking {
    public HandTypeEnum thinkHand(HandTypeEnum you);
}
