/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package janken;

import janken.Enum.ResultTypeEnum;
import janken.Enum.HandTypeEnum;

/**
 *
 * @author usuda2102
 */
public class Janken {
    public static ResultTypeEnum umpireYou(HandTypeEnum you,HandTypeEnum com)
    {
        if(you == com){
            return ResultTypeEnum.DRAW;
        }else if((you == HandTypeEnum.GOO) && (com == HandTypeEnum.CHOKI)){
            return ResultTypeEnum.WIN;
        }else if((you == HandTypeEnum.CHOKI) && (com == HandTypeEnum.PAR)){
            return ResultTypeEnum.WIN;
        }else if((you == HandTypeEnum.PAR) && (com == HandTypeEnum.GOO)){
            return ResultTypeEnum.WIN;
        }else{
            return ResultTypeEnum.LOSE;
        }
    }
}
