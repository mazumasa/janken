/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package janken;

import janken.Interface.IJanken;
import janken.Enum.HandTypeEnum;

/**
 *
 * @author usuda2102
 */
public class You implements IJanken {
    HandTypeEnum yourHand;
    
    public You(HandTypeEnum yourHand){
        this.yourHand = yourHand;
    }
    
    public HandTypeEnum showHand(){
        return yourHand;
    }
}
