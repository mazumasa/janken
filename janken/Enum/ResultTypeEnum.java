/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package janken.Enum;

/**
 *
 * @author usuda2102
 */
public enum ResultTypeEnum {
    WIN("あなたの勝ちです。"),
    LOSE("あなたの負けです。"),
    DRAW("あいこです。");
    
    private String ResultMsg;
    ResultTypeEnum(String msg){this.ResultMsg = msg;}
    public String getResultMsg(){return this.ResultMsg;}
}