/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package janken.Enum;

import java.util.Random;

/**
 *
 * @author usuda2102
 */
public enum HandTypeEnum {
    GOO("/resources/s_goo.jpg"),
    CHOKI("/resources/s_choki.jpg"),
    PAR("/resources/s_par.jpg");
    
    private String imgSrc;
    HandTypeEnum(String imgSrc){this.imgSrc = imgSrc;}
    public String getImgSrc(){return this.imgSrc;}
    public static HandTypeEnum getRandomHand(){
        Random rdm = new Random();
        switch(rdm.nextInt(6)%3){
            case 0:
                return HandTypeEnum.CHOKI;
            case 1:
                return HandTypeEnum.GOO;
            case 2:
                return HandTypeEnum.PAR;
            default:
                throw new RuntimeException("想定外の乱数が選択されました。");
        }
    }
    
    public static HandTypeEnum getWinHand(HandTypeEnum hand){
        if(hand == HandTypeEnum.CHOKI){
            return HandTypeEnum.GOO;
        }else if(hand == HandTypeEnum.GOO){
            return HandTypeEnum.PAR;
        }else if(hand == HandTypeEnum.PAR){
            return HandTypeEnum.CHOKI;
        }else{
            throw new RuntimeException("想定外のHandTypeEnumが与えられました。");
        }
    }
}
