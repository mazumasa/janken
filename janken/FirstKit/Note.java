/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package janken.FirstKit;

import java.util.ArrayList;
import janken.Enum.HandTypeEnum;

/**
 *
 * @author usuda2102
 */
public class Note {
    private int max;
    private ArrayList handList;
    
    public Note(int maxPage){
        this.max = maxPage;
        this.handList = new ArrayList();
        this.initPage();
    }
    
    public int getMax(){
        return this.max;
    }
    
    public void write(HandTypeEnum hand){
        if(this.handList.size() < this.max){
            this.handList.add(hand);
        }else{
            this.handList.remove(0);
            this.handList.add(hand);
        }       
    }
    
    public HandTypeEnum read(int page){
        if(page > this.max) throw new RuntimeException("最大ページを超えるページ数は指定できません。");
        return (HandTypeEnum) this.handList.get(page);
    }
    
    public HandTypeEnum readWinHand(int page){
        return HandTypeEnum.getWinHand((HandTypeEnum) this.read(page));
    }
    
    private void initPage(){
        for(int i=0;i <= this.max;i++){
            this.handList.add(HandTypeEnum.getRandomHand());
        }
    }
    
}
