/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package janken.FirstKit;

import java.util.Random;
import janken.Enum.HandTypeEnum;

/**
 *
 * @author usuda2102
 */
public class Dice {
    private int faceNum;
    
    public Dice(){
        this.faceNum = 6;
    }
    
    public Dice(int num){
        if(num <= 0) throw new RuntimeException("0は指定できません。");
        this.faceNum = num;
    }
    
    private HandTypeEnum numToHand(int num){
        HandTypeEnum hand;
        switch(num%3){
            case 0:
                hand = HandTypeEnum.GOO;
                break;
            case 1:
                hand = HandTypeEnum.CHOKI;
                break;
            case 2:
                hand = HandTypeEnum.PAR;
                break;
            default:
                throw new RuntimeException("想定外のHandTypeEnumです。");
        }
        return hand;
    }
	
    public int rollNum(){
        Random rdm = new Random();
        return rdm.nextInt(this.faceNum);
    }
    
    public HandTypeEnum rollHand(){
    	return numToHand(this.rollNum());
    }
}
