/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package janken;

import janken.Interface.IJanken;
import janken.Interface.IThinking;
import janken.Enum.HandTypeEnum;

/**
 *
 * @author usuda2102
 */
public class Com implements IJanken,IThinking {
    private HandTypeEnum handType;
      
    public HandTypeEnum showHand()
    {        
        return this.handType;
    }
    //このメソッドを実装する
    public HandTypeEnum thinkHand(HandTypeEnum you){
        this.handType = HandTypeEnum.PAR;
        return this.handType;
    }
}
